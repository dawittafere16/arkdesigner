import React from "react";
import Typography from "@mui/material/Typography";
import GitHubIcon from "@mui/icons-material/GitHub";
import { motion } from "framer-motion";
import { Stack } from "@mui/material";

const Footer = () => {
  return (
    <Stack
      position={{ xs: "sticky", md: "absolute" }}
      left={16}
      bottom={16}
      direction="row"
      alignItems="center"
      marginTop="3rem"
    >
      <Typography variant="body1" fontWeight={500} height={30}>
        <a href="https://www.arktides.com/" target="_blank" rel="noreferrer">
          Built by Arktides
        </a>
      </Typography>
     
    </Stack>
  );
};

export default Footer;
