interface EndWallProps {
  color: string;
  position: number[];
  rotate: boolean;
  twoStory: boolean;
}

const EndWall = ({ color, position, rotate, twoStory }: EndWallProps) => {
  const rotation = rotate ? -Math.PI : 0;
  const endHeight = twoStory ? 14.75 : 7.75;
  const endBoxHeight = twoStory ? 26.5 : 13.5;
  const topHeight = twoStory ? 27.5 : 13.5

  return (
    <mesh position={position} rotation={[0, rotation, 0]}>

      {/* two vertical colons */}
      <mesh position={[-6.775, endHeight, 9.75]}>
        <boxGeometry args={[0.55, endBoxHeight, 1]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>
      <mesh position={[-6.775, endHeight, -9.75]}>
        <boxGeometry args={[0.55, endBoxHeight, 1]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>
      {/* horizontal top colon */}
      <mesh position={[-6.71, topHeight, 0]}>
        <boxGeometry args={[0.68, 2, 18.5]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>
      <mesh position={[-6.71, 3, 0]}>
        <boxGeometry args={[0.68, 1, 18.5]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>
    </mesh>
  );
};

export default EndWall;
