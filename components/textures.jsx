import React, { useRef, useEffect, useMemo } from 'react';
import { useTexture } from '@react-three/drei';

const TextureModel = ({ texturePath, position, scale }) => {
  const texture = useTexture(texturePath);
  const ref = useRef();

  useEffect(() => {
    if (ref.current) {
      // Any additional setup or logic you want to perform
    }
  }, []);

  useMemo(() => {
    // Any additional setup or logic you want to perform
  }, []);

  return (
    <mesh ref={ref} position={position} scale={scale}>
      {/* <planeGeometry args={[30, 30]} />
      <meshBasicMaterial map={texture} /> */}
    </mesh>
  );
};

export default TextureModel;
