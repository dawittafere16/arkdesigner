import React, { useRef, useEffect } from 'react';
import { useFrame } from '@react-three/fiber';
import * as THREE from 'three';

// ArrowControl component
export const ArrowControl = ({ direction, position }) => {
  const arrowRef = useRef();

  useEffect(() => {
    if (arrowRef.current) {
      arrowRef.current.rotation.set(0, 0, 0);
      switch (direction) {
        case 'up':
          arrowRef.current.rotation.x = Math.PI / 2;
          break;
        case 'down':
          arrowRef.current.rotation.x = -Math.PI / 2;
          break;
        case 'left':
          arrowRef.current.rotation.y = Math.PI / 2;
          break;
        case 'right':
          arrowRef.current.rotation.y = -Math.PI / 2;
          break;
        default:
          break;
      }
      arrowRef.current.position.set(...position);
    }
  }, [direction, position]);

  return (
    <mesh ref={arrowRef} position={[0, 0, 0]}>
      <coneGeometry args={[0.5, 1, 4]} />
      <meshBasicMaterial color="blue" />
    </mesh>
  );
};

// RotateControl component
export const RotateControl = ({ direction, position }) => {
  const rotateRef = useRef();

  useFrame(() => {
    if (rotateRef.current) {
      switch (direction) {
        case 'left':
          rotateRef.current.rotation.z += 0.01;
          break;
        case 'right':
          rotateRef.current.rotation.z -= 0.01;
          break;
        default:
          break;
      }
    }
  });

  useEffect(() => {
    if (rotateRef.current) {
      rotateRef.current.position.set(...position);
    }
  }, [position]);

  return (
    <mesh ref={rotateRef} position={[0, 0, 0]}>
      <cylinderGeometry args={[0.5, 0.5, 0.2, 32]} />
      <meshBasicMaterial color="red" />
    </mesh>
  );
};
