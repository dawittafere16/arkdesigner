interface WallProps {
  color: string;
  position: number[];
}

const Wall = ({ color, position }: WallProps) => {
  return (
    <>
      <mesh position={position} castShadow receiveShadow>
        <boxGeometry args={[19, 12, 0.5]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>
      <mesh position={position} castShadow receiveShadow>
        <boxGeometry args={[19.8, 12, 0.625]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>
    </>
  );
};

export default Wall;
