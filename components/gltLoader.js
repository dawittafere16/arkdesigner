import React, { useRef, useMemo, useEffect, useState } from 'react';
import { useFrame, useLoader } from '@react-three/fiber';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader';
import * as THREE from 'three';

const plane = new THREE.Plane();
const intersection = new THREE.Vector3();
const offset = new THREE.Vector3();
const inverseMatrix = new THREE.Matrix4();

const GltfModel = ({ modelPath, position, scale, color }) => {
  const gltf = useLoader(GLTFLoader, modelPath);
  const ref = useRef();
  const [dragging, setDragging] = useState(false);

  useEffect(() => {
    const handleKeyDown = (e) => {
      switch (e.key) {
        case 'ArrowUp':
          ref.current.position.y += 1;
          break;
        case 'ArrowDown':
          ref.current.position.y -= 1;
          break;
        case 'ArrowLeft':
          ref.current.position.x -= 1;
          break;
        case 'ArrowRight':
          ref.current.position.x += 1;
          break;
        case 'r':
          // Rotate right
          ref.current.rotation.y += 0.1;
          break;
        case 'l':
          // Rotate left
          ref.current.rotation.y -= 0.1;
          break;
        default:
          break;
      }
    };

    window.addEventListener('keydown', handleKeyDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, []);

  useFrame(({ raycaster, camera }) => {
    if (dragging && raycaster) {
      // Check and update intersection
      if (raycaster.ray.intersectPlane(plane, intersection)) {
        // Your custom logic for updating the position during dragging
        ref.current.position.copy(intersection.sub(offset).applyMatrix4(inverseMatrix));
        // Prevent controls from affecting the camera during dragging
        camera.position.set(camera.position.x, camera.position.y, camera.position.z);
      }
    }
  });

  useEffect(() => {
    if (ref.current) {
      traverseAndUpdateColor(ref.current, color);
    }
  }, [color]);

  useMemo(() => {
    if (gltf.animations && gltf.animations.length) {
      const mixer = new THREE.AnimationMixer(ref.current);
      const actions = gltf.animations.map((clip) => mixer.clipAction(clip));
      actions.forEach((action) => action.play());

      useFrame((state, delta) => {
        mixer.update(delta);
      });
    }
  }, [gltf.animations]);

  const events = {
    onPointerDown: (e) => {
      e.stopPropagation();
      // Add your custom logic for onPointerDown
      setDragging(true);
      // Store initial offset
      offset.copy(e.point).sub(ref.current.position);
    },
    onPointerUp: () => {
      // Add your custom logic for onPointerUp
      setDragging(false);
    },
  };

  return (
    <>
      <mesh ref={ref} position={position} scale={scale} castShadow receiveShadow {...events}>
        <primitive object={gltf.scene} />
      </mesh>
      {/* Custom arrow controls */}
      <ArrowControl direction="up" position={[position[0], position[1] + 2, position[2]]} />
      <ArrowControl direction="down" position={[position[0], position[1] - 2, position[2]]} />
      <ArrowControl direction="left" position={[position[0] - 2, position[1], position[2]]} />
      <ArrowControl direction="right" position={[position[0] + 2, position[1], position[2]]} />
      <RotateControl direction="left" position={[position[0], position[1], position[2] - 2]} />
      <RotateControl direction="right" position={[position[0], position[1], position[2] + 2]} />
    </>
  );
};

// Helper function to traverse the object hierarchy and update colors
const traverseAndUpdateColor = (object, color) => {
  object.traverse((child) => {
    if (child.isMesh) {
      if (child.material) {
        child.material.color.set(color);
      } else if (child.material.length) {
        child.material.forEach((material) => {
          material.color.set(color);
        });
      }
    }
  });
};

// Custom arrow control component
const ArrowControl = ({ direction, position }) => {
  const arrowRef = useRef();

  useFrame(() => {
    // You can add custom logic for arrow controls here if needed
  });

  return (
    <mesh ref={arrowRef} position={position}>
      {/* Create a custom arrow geometry/material here */}
      {/* Example: <arrowGeometry /> <arrowMaterial /> */}
    </mesh>
  );
};

// Custom rotate control component
const RotateControl = ({ direction, position }) => {
  const rotateRef = useRef();

  useFrame(() => {
    // You can add custom logic for rotate controls here if needed
  });

  return (
    <mesh ref={rotateRef} position={position}>
      {/* Create a custom rotate control geometry/material here */}
      {/* Example: <rotateGeometry /> <rotateMaterial /> */}
    </mesh>
  );
};

export default GltfModel;
