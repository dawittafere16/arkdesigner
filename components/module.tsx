import { useTexture } from "@react-three/drei";
import Wall from "./wall";
import { COLORS } from "./colors";
import { TEXTURES } from "./materials";

interface ModuleProps {
  color: string;
  position: number[];
  envelopeOn: boolean;
  floorMaterial: string;
}

const Module = ({
  color,
  position,
  envelopeOn,
  floorMaterial,
}: ModuleProps) => {
  const texture = (TEXTURES as any)[floorMaterial];
  const floorMaterialProps = useTexture(texture);

  return (
    <mesh position={position}>
      {envelopeOn && <Wall color={color} position={[0, 8.75, 10]}></Wall>}
      {/* side walls */}
      <mesh position={[0, 1.5, 0]} castShadow receiveShadow>
        <boxGeometry args={[19.75, 2, 45]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>

for future Deck
{/* colored floor marble */}
      <mesh position={[0, 2.5, 0]} rotation={[0, -Math.PI / 2, 0]}>
        <boxGeometry args={[19, 0.1, 18]} />
        <meshStandardMaterial {...floorMaterialProps} />
      </mesh>

      {/* top colored floor */}
      <mesh position={[0, 13.9, 0]} rotation={[0, -Math.PI / 2, 0]}>
        <boxGeometry args={[19, 0.1, 20]} />
        <meshStandardMaterial
          color={COLORS.gyp}
          metalness={0.1}
          roughness={0.1}
        />
      </mesh>
{/* vertical colons side one */}
      {/* <mesh position={[9.625, -0.2, 9.75]} castShadow receiveShadow>
        <boxGeometry args={[0.5, 7.9, 0.5]} />
        <meshStandardMaterial color={"red"} metalness={0.5} roughness={0.5} />
      </mesh>
      <mesh position={[-9.625, -0.05, 9.75]} castShadow receiveShadow>
        <boxGeometry args={[0.5, 7.9, 0.5]} />
        <meshStandardMaterial color={"green"} metalness={0.5} roughness={0.5} />
      </mesh> */}
{/* mesh bettwen the colons one side####################################################################3*/}
      {/* <mesh position={[0, -0.05, 9.5]} receiveShadow>
        <boxGeometry args={[20, 7.9, 0.05]} />
        <meshStandardMaterial
          color={COLORS.gyp}
          metalness={0.1}
          roughness={0.1}
        />
      </mesh> */}

      {/* mesh bettwen the colons second side#############################################################*/}

      {/* <mesh position={[0, -0.05, -9.5]} receiveShadow>
        <boxGeometry args={[20, 7.9, 0.05]} />
        <meshStandardMaterial
          color={COLORS.gyp}
          metalness={0.1}
          roughness={0.1}
        />
      </mesh> */}
{/* center vertical colons #########################################################################*/}
      {/* <mesh position={[3.625, -0.05, 0]} castShadow receiveShadow>
        <boxGeometry args={[0.5, 70.9, 0.5]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>

      <mesh position={[-3.625, -0.05, 0]} castShadow receiveShadow>
        <boxGeometry args={[0.5, 7.9, 0.5]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh> */}
      
{/* vertical colons side two */}

      <mesh position={[9.625, 10.05, -9.75]} castShadow receiveShadow>
        <boxGeometry args={[0.5, 7.9, 0.5]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>
      <mesh position={[-9.625, 10.05, -9.75]} castShadow receiveShadow>
        <boxGeometry args={[0.5, 7.9, 0.5]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>

{/* top colored */}
      <mesh position={[0, 14.4, 0]} castShadow receiveShadow>
        <boxGeometry args={[19.75, 1, 20]} />
        <meshStandardMaterial color={color} metalness={0.5} roughness={0.5} />
      </mesh>
      {envelopeOn && <Wall color={color} position={[0, 8.75, -10]}></Wall>}
    </mesh>
  );
};

export default Module;
