import React, { FC, useState, useEffect, Suspense,useMemo } from "react";
import * as THREE from 'three'
import { useRouter } from "next/router";
import { Canvas,extend, useThree, useLoader, useFrame } from "@react-three/fiber";
import Module from "../../components/module";
import Foundation from "../../components/foundation";
import Roof from "../../components/roof";
import EndWall from "../../components/endwall";
import NavBar from "../../components/navbar";
import Colors, { COLORS } from "../../components/colors";
import Materials from "../../components/materials";
import Loader from "../../components/loader";
import { OrbitControls, Sky, Stage, Environment,  } from "@react-three/drei";
import Ocean from '../../components/ocean'; 
import { useSpring, animated, config } from "@react-spring/web";
import { Typography, useMediaQuery, Button } from "@mui/material";
import { IconLogin, IconLogout } from "@tabler/icons-react";
import GltfModel from '../../components/gltLoader';
import TextureModel from '../../components/textures'
import { color } from "framer-motion";

const Homes: FC = () => {
  const router = useRouter();
  const envelopeOn = true;
  const [isInside, setIsInside] = useState(false);
  const [currentArea, setCurrentArea] = useState(0);
  const [homeColor, setHomeColor] = useState(COLORS.white);
  const [floorMaterial, setFloorMaterial] = useState("walnut");
  const isMobile = useMediaQuery("(max-width:900px)");

  let foundationWidth;
  const w = router.query.home?.slice(3, 4);
  switch (w) {
    case "1":
      foundationWidth = 10;
      break;
    case "2":
      foundationWidth = 15;
      break;
    case "3":
      foundationWidth = 33;
      break;
    case "4":
      foundationWidth = 41;
      break;
    case "5":
      foundationWidth = 59;
      break;
  }

  let roofHeight;
  const stories = router.query.home?.slice(5);
  switch (stories) {
    case "1":
      roofHeight = 5.3;
      break;
    case "2":
      roofHeight = 15.3;
      break;
  }

  let home;
  switch (router.query.home) {
    case "mod1x1":
      home = (
        <>
          {envelopeOn && (
            <EndWall
              position={[-3, 0, 0]}
              color={homeColor}
              rotate={false}
              twoStory={false}
            />
          )}
          <Module
            position={[0, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          {envelopeOn && (
            <EndWall
              position={[3, 0, 0]}
              color={homeColor}
              rotate
              twoStory={false}
            />
          )}
        </>
      );
      break;
    case "mod2x1":
      home = (
        <>
          {envelopeOn && (
            <EndWall
              position={[-12.9, 0, 0]}
              color={homeColor}
              rotate={false}
              twoStory={false}
            />
          )}
          <Module
            position={[-10, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[10, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          {envelopeOn && (
            <EndWall
              position={[12.9, 0, 0]}
              color={homeColor}
              rotate
              twoStory={false}
            />
          )}
        </>
      );
      break;
    case "mod3x1":
      home = (
        <>
          {envelopeOn && (
            <EndWall
              position={[-23, 0, 0]}
              color={homeColor}
              rotate={false}
              twoStory={false}
            />
          )}
          <Module
            position={[-20, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[0, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[20, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          {envelopeOn && (
            <EndWall
              position={[23, 0, 0]}
              color={homeColor}
              rotate
              twoStory={false}
            />
          )}
        </>
      );
      break;
    case "mod4x1":
      home = (
        <>
          {envelopeOn && (
            <EndWall
              position={[-15.5, 0, 0]}
              color={homeColor}
              rotate={false}
              twoStory={false}
            />
          )}
          <Module
            position={[-12, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[-4, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[4, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[12, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          {envelopeOn && (
            <EndWall
              position={[15.5, 0, 0]}
              color={homeColor}
              rotate
              twoStory={false}
            />
          )}
        </>
      );
      break;
    case "mod5x1":
      home = (
        <>
          {envelopeOn && (
            <EndWall
              position={[-19.5, 0, 0]}
              color={homeColor}
              rotate={false}
              twoStory={false}
            />
          )}
          <Module
            position={[-16, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[-8, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[0, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[8, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[16, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          {envelopeOn && (
            <EndWall
              position={[19.5, 0, 0]}
              color={homeColor}
              rotate
              twoStory={false}
            />
          )}
        </>
      );
      break;
    case "mod2x2":
      home = (
        <>
          {envelopeOn && (
            <EndWall
              position={[-7.5, 0, 0]}
              color={homeColor}
              rotate={false}
              twoStory
            />
          )}
          <Module
            position={[-4, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[4, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[-4, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[4, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          {envelopeOn && (
            <EndWall position={[7.5, 0, 0]} color={homeColor} rotate twoStory />
          )}
        </>
      );
      break;
    case "mod3x2":
      home = (
        <>
          {envelopeOn && (
            <EndWall
              position={[-11.5, 0, 0]}
              color={homeColor}
              rotate={false}
              twoStory
            />
          )}
          <Module
            position={[-8, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[0, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[8, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[-8, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[0, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[8, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          {envelopeOn && (
            <EndWall
              position={[11.5, 0, 0]}
              color={homeColor}
              rotate
              twoStory
            />
          )}
        </>
      );
      break;
    case "mod4x2":
      home = (
        <>
          {envelopeOn && (
            <EndWall
              position={[-15.5, 0, 0]}
              color={homeColor}
              rotate={false}
              twoStory
            />
          )}
          <Module
            position={[-12, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[-4, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[4, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[12, 0, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[-12, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[-4, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[4, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          <Module
            position={[12, 14, 0]}
            color={homeColor}
            envelopeOn={envelopeOn}
            floorMaterial={floorMaterial}
          ></Module>
          {envelopeOn && (
            <EndWall
              position={[15.5, 0, 0]}
              color={homeColor}
              rotate
              twoStory
            />
          )}
        </>
      );
      break;
  }

 


  
 


  const floorArea = useSpring({
    val: currentArea,
    from: 0,
    config: config.molasses,
    clamp: true,
  }) as any;

  useEffect(() => {
    if (router.query.home === "mod1x1") {
      setCurrentArea(320 * 1);
    } else if (router.query.home === "mod2x1") {
      setCurrentArea(320 * 2);
    } else if (router.query.home === "mod2x1") {
      setCurrentArea(320 * 2);
    } else if (router.query.home === "mod3x1") {
      setCurrentArea(320 * 3);
    } else if (router.query.home === "mod4x1") {
      setCurrentArea(320 * 4);
    } else if (router.query.home === "mod5x1") {
      setCurrentArea(320 * 5);
    } else if (router.query.home === "mod2x2") {
      setCurrentArea(320 * 4);
    } else if (router.query.home === "mod3x2") {
      setCurrentArea(320 * 6);
    } else if (router.query.home === "mod4x2") {
      setCurrentArea(320 * 8);
    }
  }, [router.query.home]);

  const handleColorChange = (color: string) => {
    setHomeColor(color);
  };

  const handleFloorMaterialChange = (floorMaterial: string) => {
    setFloorMaterial(floorMaterial);
  };

  const [position, setPosition] = useState([10, 0, 0]);

  const handleMove = (direction: string) => {
    // Update the position state based on the desired direction
    switch (direction) {
      case "left":
        setPosition((prevPosition) => [prevPosition[0] - 1, prevPosition[1], prevPosition[2]]);
        break;
      case "right":
        setPosition((prevPosition) => [prevPosition[0] + 1, prevPosition[1], prevPosition[2]]);
        break;
      case "up":
        setPosition((prevPosition) => [prevPosition[0], prevPosition[1] + 1, prevPosition[2]]);
        break;
      case "down":
        setPosition((prevPosition) => [prevPosition[0], prevPosition[1] - 1, prevPosition[2]]);
        break;
      // Add more cases as needed
      default:
        break;
    }
  };

  const [maxPolarAngle, setMaxPolarAngle] = useState(Math.PI / 2);
  const [maxDistance, setMaxDistance] = useState(100);
  const [enableZoom, setEnableZoom] = useState(true);
  const [cameraPosition,setcameraPosition] =  useState([0, 0, 0]);

  

const handleInsideToggle = () => {
  setIsInside(prevIsInside => !prevIsInside);

  if (typeof window !== "undefined") {
    if (!isInside) {
      window.localStorage.setItem("position", "outside");
      setMaxDistance(100);
      setEnableZoom(true);
      setcameraPosition([60,0,60])
      
    } if(isInside) {
      const td = localStorage.getItem('position')
      if(td === 'inside')
      {
      window.localStorage.setItem("position", "outside");
      setMaxDistance(1000);
      setEnableZoom(false);
      setcameraPosition([600,1000,60])
     
      }
      if(td === 'outside')
      {
        window.localStorage.setItem("position", "inside");
      }
    }
  }
};
useEffect(() => {
  if (localStorage.getItem("position") === "inside") {
    window.localStorage.setItem("position", "outside");
    setIsInside(true);
   
    setcameraPosition([60000, 10000, 60]);
  } else {
    setIsInside(false);
    
    setcameraPosition([60, 0, 60]);
  }
}, []);




  return (
    <>
      <div
        style={{ position: "relative", maxWidth: "1200px", margin: "0 auto" }}
      >
        <NavBar />
      </div>
      <div style={{ height: "100vh", width: "100vw", margin: 0 }}>
   
          <Canvas
            camera={{
              position:cameraPosition,
              fov: isMobile ? 70 : 40,
            }}
            shadows
          >
            
            <Suspense fallback={<Loader />}>
     
            <OrbitControls
          maxPolarAngle={maxPolarAngle}
          maxDistance={maxDistance}
          enableZoom={enableZoom}
         

        />
              
              <ambientLight intensity={0.5} />
              <pointLight position={[100, 100, 20]} />
              <Sky
                sunPosition={[100, 100, 20]}
                inclination={0}
                azimuth={0.25}
              />
              
              <Stage
                intensity={0.4}
                environment="city"
                adjustCamera={false}
                shadows={{
                  type: "accumulative",
                  color: COLORS.shadow,
                  opacity: 0.3,
                }}
              >
              
                <Ocean />
                 {/* Your Texture model */}
   

                <GltfModel modelPath="/images/models/lieutenantHead/lieutenantHead.gltf"  
    // Handle drag event
 position={position} color={color}  scale={[1, 1, 1]}  />


{/* <             Foundation
                  position={[0, 11, 0]}
                  color={COLORS.foundation}
                  width={foundationWidth}
                /> */}
                {home}
                <Foundation
                  position={[0, -0.5, 0]}
                  color={COLORS.foundation}
                  width={foundationWidth}
                />
                <Roof
                  position={[0, roofHeight || 0, 0]}
                  color={COLORS.roof}
                  width={foundationWidth}
                />
              </Stage>
            </Suspense>
          </Canvas>
      

        <div
          style={{
            position: "relative",
            maxWidth: "1200px",
            margin: "0 auto",
          }}
        >
          {isInside ? (
            <Materials
              floorMaterial={floorMaterial}
              handleFloorMaterial={handleFloorMaterialChange}
            />
          ) : (
            <Colors homeColor={homeColor} handleColor={handleColorChange} />

          )}
      <Button onClick={() => handleMove("left")}>Move Left</Button>
      <Button onClick={() => handleMove("right")}>Move Right</Button>
      <Button onClick={() => handleMove("up")}>Move Up</Button>
      <Button onClick={() => handleMove("down")}>Move Down</Button>
          <Button
            size="large"
            variant="text"
            onClick={handleInsideToggle}
            sx={{
              position: "absolute",
              height: isMobile ? 58 : 68,
              bottom: isMobile ? 86 : 16,
              left: isMobile ? 16 : 240,
              color: "#131414",
              border: "1px dashed #131414",
              backdropFilter: "blur(5px)",
            }}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <div style={{ marginRight: "0.5rem" }}>
                {isInside ? "Go Outside" : "Go Inside"}
              </div>
              {isInside ? <IconLogout /> : <IconLogin />}
            </div>
          </Button>

          <Typography
            variant="h6"
            position="absolute"
            color="#131414"
            padding={{ xs: "0.75rem", sm: "1rem" }}
            sx={{
              right: 16,
              bottom: 16,
              margin: 0,
              border: "1px dashed #131414",
              background: "none",
              borderRadius: "0.5rem",
              backdropFilter: "blur(5px)",
            }}
          >
            <animated.span>
              {floorArea.val.to((val: number) =>
                new Intl.NumberFormat().format(Math.floor(val))
              )}
            </animated.span>{" "}
            sf
          </Typography>
        </div>
      </div>
    </>
  );
};

export default Homes;
